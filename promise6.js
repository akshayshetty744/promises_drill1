//6. Write a funtion named `wait` that accepts `time` in ms and executes
//the function after the given time.

function wait(time) {
    let promise = new Promise(function (resolve, reject) {
       setTimeout(() => {
           resolve("Promise Resolved");
           reject("Promise Failed");
       }, time); 
    })
    promise.then((result) =>console.log(result)).catch((err) =>console.log(err))
}
wait(2000)