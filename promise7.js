//7. Write a basic implementation of `Promise.all` that accepts
// an array of promises and return another array with the data
//coming from all the promises.Make sure if any of the Promise gets rejected
//throw error.Only when all the promises are fulfilled resolve the promise.

let promise1 = new Promise(function (resolve, reject) {
        resolve("Promise Resolved")
})
let promise2 = new Promise(function (resolve, reject) {
          reject("Promise Failed")
})
let promise3 = new Promise(function (resolve, reject) {
        resolve("Promise3 Resolved");
})
Promise.all([promise1, promise2, promise3])
            .then((result) => { console.log(result) })
            .catch((err) => { console.log(err) })