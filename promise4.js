//4. What will be the output of the code below.

let a = console.log('A');

// Asynchronous code finises in 0 seconds (Callback Queue)
let b = setTimeout(() => console.log('B'), 0);

// A promise that resolves right away (Microtask Queue)
let c = Promise.resolve().then(() => console.log('C'));

let d =console.log('D');
d = new Promise(function (resolve, reject) {
    reject("error")
}).catch()
Promise.all([a,c,d,b]).then((res)=> console.log(res))
// output : A D C B