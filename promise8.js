 // 8. Use this data endpoint to get the data and console the each house names and handle the error as well.
 // [ENDPOINT](https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json)

  //  - Use fetch to get data.
  //  - Handle if the user is not connected to internet.
  //  - Handle error that may occure while fetching data.
fetch( "https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json")
  .then((response) => response.json())
    .then((data) => {
        let result = data.houses;
        for (let index in result) {
            console.log(data.houses[index].name)
        }
        if (!data.akshay) {
            console.log("Online")
        }
  })
    .catch(() => {
        console.log('Offline')
    }
);

// var XMLHttpRequest = require("xhr2");
//   let xhr = new XMLHttpRequest();
//   xhr.open("GET","https://raw.githubusercontent.com/nnnkit/json-data-collections/master/got-houses.json");
//   xhr.responseType = "json";
//   xhr.send();
// xhr.onload = function () {
//         let responseObj = xhr.response;
//         console.log(responseObj.houses[0].people);
//   };
